local bufferline = require('bufferline')
require("bufferline").setup {
  options = {
    style_preset = bufferline.style_preset.default,
    diagnostics = "nvim_lsp",
    diagnostics_indicator = function(count, level, diagnostics_dict, context)
      local s = " "
      for e, n in pairs(diagnostics_dict) do
        local sym = e == "error" and " "
            or (e == "warning" and " " or "")
        s = s .. n .. sym
      end
      return s .. count .. level .. context
    end,
    offsets = {
      {
        filetype = "NvimTree",
        text = "File Explorer",
        text_align = "left",
        separator = true
      }
    },
    separator_style = "slant",
    sort_by = "insert_after_current",
    integrations = {
      cmp = true,
      gitsigns = true,
      nvimtree = true,
      telescope = true,
      notify = true,
    },
    highlights = require("catppuccin.groups.integrations.bufferline").get()
  }
}
