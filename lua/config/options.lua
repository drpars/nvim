local set = vim.opt
local o = vim.o

-- Map <leader> = the space key
vim.g.mapleader = ' '
vim.g.maplocalleader = " "

-- File Explorer nvim-tree
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

-- Editor options
o.number = true
o.relativenumber = true
o.clipboard = "unnamedplus"
o.syntax = "on"
o.autoindent = true
set.list = true
--set.listchars:append "eol:↴"
o.cursorline = true
o.expandtab = true
o.shiftwidth = 2
o.tabstop = 2
o.encoding = "UTF-8"
o.ruler = true
o.mouse = "a"
o.title = true
o.hidden = true
o.ttimeoutlen = 0
o.wildmenu = true
o.showcmd = true
o.showmatch = true
o.termguicolors = true

vim.cmd(
  -- Colorscheme
  -- duskfox | rose-pine | nightfox | nordfox | 
  -- catppuccin-latte | catppuccin-frappe | catppuccin-macchiato | catppuccin-mocha
  "colorscheme catppuccin-mocha"
)

