-- Shorten function name
local keymap = vim.keymap.set
-- Silent keymap option
local opts = { silent = true }

--Remap space as leader key
keymap("", "<Space>", "<Nop>", opts)

-- Modes
--   normal_mode = "n",
--   insert_mode = "i",
--   visual_mode = "v",
--   visual_block_mode = "x",
--   term_mode = "t",
--   command_mode = "c",

-- Normal --
-- Better Window navigation
keymap("n", "<C-h>", "<C-w>h", { desc = "Navigate Left" }, opts)
keymap("n", "<C-j>", "<C-w>j", { desc = "Navigate Down" }, opts)
keymap("n", "<C-k>", "<C-w>k", { desc = "Navigate Up" },opts)
keymap("n", "<C-l>", "<C-w>l", { desc = "Navigate Right" },opts)

-- move command
keymap("v", "J", ":m '>+1<cr>gv=gv", { desc = "Move line or word" })
keymap("v", "K", ":m '<-2<cr>gv=gv", { desc = "Move line or word" })

-- Resize with arrows
keymap("n", "<C-Up>", ":resize -2<cr>", opts)
keymap("n", "<C-Down>", ":resize +2<cr>", opts)
keymap("n", "<C-Left>", ":vertical resize -2<cr>", opts)
keymap("n", "<C-Right>", ":vertical resize +2<cr>", opts)

-- Navigate buffers
keymap("n", "<S-l>", ":bnext<cr>", { desc = "Next Buffer" }, opts)
keymap("n", "<S-h>", ":bprevious<cr>", { desc = "Previous Buffer" }, opts)

-- Clear highlights
keymap("n", "<leader>h", "<cmd>nohlsearch<cr>", { desc = "Remove Highlight Search" }, opts)

-- Close buffers
keymap("n", "<S-q>", "<cmd>Bdelete!<cr>", { desc = "Close Buffer" }, opts)

-- Visual --
-- Stay in indent mode
keymap("v", "<", "<gv", opts)
keymap("v", ">", ">gv", opts)

-- NvimTree
keymap("n", "<leader>e", ":NvimTreeToggle<cr>", { desc = "Explorer" }, opts)

-- Oil
keymap("n", "-", require("oil").open_float, { desc = "Open parent directory" })
keymap("n", "<leader>-", require("oil").close, { desc = "Close directory" })

-- Comment
keymap("n", "<leader>/", "<cmd>lua require('Comment.api').toggle.linewise.current()<cr>", { desc = "Comment" }, opts)
keymap("x", "<leader>/", '<ESC><cmd>lua require("Comment.api").toggle.linewise(vim.fn.visualmode())<cr>', { desc = "Comment" })

-- Custom
keymap("n", "<leader>n", "<cmd> tabnew<cr>", { desc = "New File" }, opts)
keymap("n", "<leader>;", "<cmd>Alpha<cr>", { desc = "Menu" }, opts)
keymap("n", "<leader>o", "<cmd>bd<cr>", { desc = "Back" }, opts)
-- TransparentToggle
keymap("n", "TT", ":TransparentToggle<cr>", { noremap = true, desc = "Make Transparent" })
-- f -- Fterm
keymap('n', '<leader>ft', '<cmd>lua require("FTerm").toggle()<cr>', { desc = "Float Term" }, opts)
keymap('t', '<leader>ft', '<C-\\><C-n><cmd>lua require("FTerm").toggle()<cr>', { desc = "Float Term" }, opts)
-- g -- Git
keymap("n", "<leader>gs", "<cmd>Git status<cr>", { desc = "Git Status" }, opts)
keymap("n", "<leader>gd", "<cmd>Gvdiffsplit<cr>", { desc = "Git Diff Vertical" }, opts)
keymap("n", "<leader>g", "<cmd>G<cr>", { desc = "Git Fugitive" }, opts)
keymap("n", "<leader>gc", "<cmd>G commit<cr>", { desc = "Git Commit" }, opts)
-- l -- Colorizer
keymap("n", "<leader>l", "<cmd>ColorizerToggle<cr>", { desc = "Colorizer" }, opts)
-- p -- Packer
keymap("n", "<leader>pc", "<cmd>PackerCompile<cr>", { desc = "Packer Compile" }, opts)
keymap("n", "<leader>ps", "<cmd>PackerSync<cr>", { desc = "Packer Sync" }, opts)
keymap("n", "<leader>pu", "<cmd>PackerUpdate<cr>", { desc = "Packer Update" }, opts)
-- s -- Telescope
keymap("n", "<leader>sc", "<cmd>Telescope colorscheme<cr>", { desc = "Colorscheme" }, opts)
keymap("n", "<leader>sf", "<cmd>Telescope find_files<cr>", { desc = "Find Files" }, opts)
keymap("n", "<leader>sh", "<cmd>Telescope help_tags<cr>", { desc = "Help" }, opts)
keymap("n", "<leader>sr", "<cmd>Telescope oldfiles<cr>", { desc = "Recent Files" }, opts)
keymap("n", "<leader>sk", "<cmd>Telescope keymaps<cr>", { desc = "Keymaps" }, opts)
-- s -- Telescope Git
keymap("n", "<leader>sgs", "<cmd>Telescope git_status<cr>", { desc = "Telescope Git Status" }, opts)
keymap("n", "<leader>sgc", "<cmd>Telescope git_commits<cr>", { desc = "Telescope Git Commits" }, opts)
keymap("n", "<leader>sgb", "<cmd>Telescope git_branches<cr>", { desc = "Telescope Git Branches" }, opts)
-- t -- TroubleToggle
keymap("n", "<leader>tt", "<cmd>TroubleToggle<cr>", { silent = true, noremap = true, desc = "Open Trobles" })
keymap("n", "<leader>tw", "<cmd>TroubleToggle workspace_diagnostics<cr>", { silent = true, noremap = true, desc = "Workspace Diagnostic" })
keymap("n", "<leader>td", "<cmd>TroubleToggle document_diagnostics<cr>", { silent = true, noremap = true, desc = "Document Diagnostic" })
keymap("n", "<leader>tl", "<cmd>TroubleToggle loclist<cr>", { silent = true, noremap = true, desc = "Trouble Locklist" })
keymap("n", "<leader>tq", "<cmd>TroubleToggle quickfix<cr>", { silent = true, noremap = true, desc = "Trouble Quickfix" })
keymap("n", "tR", "<cmd>TroubleToggle lsp_references<cr>", { silent = true, noremap = true, desc = "Lsp References" })
-- w -- windows animation
keymap("n", "<leader>w", "<cmd>WindowsToggleAutowidth<cr>", { silent = true, noremap = true, desc = "Windows Animations" })
keymap("n", "<leader>wm", "<cmd>WindowsMaximize<cr>", { silent = true, noremap = true, desc = "Maximize current window" })
-- x -- make executable file
keymap("n", "<leader>x", "<cmd>!chmod +x %<cr>", { silent = true, desc = "Make Executable" })
-- z -- ZenMode
keymap("n", "<leader>zz", "<cmd>ZenMode<cr>", { desc = "Zen Mode" }, opts)
keymap("n", "<leader>zt", "<cmd>Twilight<cr>", { desc = "Twilight Mode" }, opts)
