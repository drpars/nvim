local fn = vim.fn

-- Automatically install packer
local install_path = fn.stdpath "data" .. "/site/pack/packer/start/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then
  PACKER_BOOTSTRAP = fn.system {
    "git",
    "clone",
    "--depth",
    "1",
    "https://github.com/wbthomason/packer.nvim",
    install_path,
  }
  print "Installing packer close and reopen Neovim..."
  vim.cmd [[packadd packer.nvim]]
end

-- Use a protected call so we don't error out on first use
local status_ok, packer = pcall(require, "packer")
if not status_ok then
  return
end

-- Have packer use a popup window
packer.init {
  display = {
    open_fn = function()
      return require("packer.util").float { border = "rounded" }
    end,
  },
  git = {
    clone_timeout = 300, -- Timeout, in seconds, for git clones
  },
}

-- Install your plugins here
return require("packer").startup(function(use)
  -- My plugins here
  use { "wbthomason/packer.nvim" }
  use { "nvim-lua/plenary.nvim" }

  -- cmd & notify
  use({
    "folke/noice.nvim",
    config = function()
      require("core.noice")
    end,
    requires = {
      -- if you lazy-load any plugin below, make sure to add proper `module="..."` entries
      "MunifTanjim/nui.nvim",
      -- OPTIONAL:
      --   `nvim-notify` is only needed, if you want to use the notification view.
      --   If not available, we use `mini` as the fallback
      "rcarriga/nvim-notify",
      config = function()
        require("notify").setup({
          background_colour = "#000000",
        })
      end,
    }
  })

  -- Comment Plugin use gc or gb in visual mode
  use { 'numToStr/Comment.nvim' }

  -- File Explorer Ctrl+f
  use { "kyazdani42/nvim-web-devicons" }
  use { 'nvim-tree/nvim-tree.lua',
    config = function()
      require("nvim-tree").setup {}
    end,
    requires = { 'nvim-tree/nvim-web-devicons' }
  }
  use { 'stevearc/oil.nvim',
    config = function()
      require('oil').setup()
    end
  }

  use { 'akinsho/bufferline.nvim', tag = "*",
    config = function()
      require("core.buffer")
    end,
  }
  use { "moll/vim-bbye" }

  -- ToggleTerm
  use { "akinsho/toggleterm.nvim", tag = '*',
    config = function()
      require("toggleterm").setup {
        open_mapping = [[<c-x>]],
        shade_terminals = false
      }
    end
  }
  -- Fterm
  use { "numToStr/FTerm.nvim",
    config = function()
      require("core.fterm")
    end
  }

  -- Telescope
  use {
    'nvim-telescope/telescope.nvim', tag = '0.1.1',
    config = function()
      require("core.telescope")
    end
  }
  use { 'nvim-telescope/telescope-fzf-native.nvim', run = 'make' }

  -- Pretty status bar
  use { 'nvim-lualine/lualine.nvim',
    config = function()
      require('lualine').setup {
        options = {
          icons_enabled = true,
          theme = 'nord',
        },
      }
    end
  }

  -- smarter indent
  use { "lukas-reineke/indent-blankline.nvim",
    config = function()
      require("indent_blankline").setup {
        space_char_blankline = " ",
        show_current_context = true,
        show_current_context_start = true,
        show_end_of_line = true,
      }
    end,
  }

  -- Treesitter
  use({
    "nvim-treesitter/nvim-treesitter",
    run = function()
      require("nvim-treesitter.install").update({ with_sync = true })
    end,
    config = function()
      require("core.treesitter")
    end,
  })
  use { "windwp/nvim-ts-autotag", after = "nvim-treesitter" }

  -- LSP
  -- lsp-zero
  use {
    'VonHeikemen/lsp-zero.nvim',
    branch = 'v2.x',
    config = function()
      require("core.lsp-zero")
    end,
    requires = {
      -- LSP Support
      { 'neovim/nvim-lspconfig' }, -- Required
      {
        -- Optional
        'williamboman/mason.nvim',
        run = function()
          pcall(require, vim.cmd, 'MasonUpdate')
        end,
      },
      { 'williamboman/mason-lspconfig.nvim' }, -- Optional

      -- Autocompletion
      { 'hrsh7th/nvim-cmp' },         -- Required
      { 'hrsh7th/cmp-nvim-lsp' },     -- Required
      { 'hrsh7th/cmp-buffer' },       -- Optional
      { 'hrsh7th/cmp-path' },         -- Optional
      { 'saadparwaiz1/cmp_luasnip' }, -- Optional
      { 'hrsh7th/cmp-nvim-lua' },     -- Optional

      -- Snippets
      { 'L3MON4D3/LuaSnip' },             -- Required
      { 'rafamadriz/friendly-snippets' }, -- Optional
    }
  }

  use {
    "folke/trouble.nvim",
    config = function()
      require("trouble").setup {
        -- your configuration comes here
        -- or leave it empty to use the default settings
        -- refer to the configuration section below
      }
    end
  }

  use { 'j-hui/fidget.nvim',
    config = function()
      require('fidget').setup()
    end
  }

  -- Git
  use { 'lewis6991/gitsigns.nvim',
    config = function()
      require("core.gitsigns")
    end
  }
  use { 'tpope/vim-fugitive' }

  -- Formatter
  use { "jose-elias-alvarez/null-ls.nvim" }
  use { 'jay-babu/mason-null-ls.nvim' }
  use {
    "windwp/nvim-autopairs",
    config = function()
      require("nvim-autopairs").setup {
        check_ts = true,
      }
    end
  }
  use { 'tpope/vim-surround' }
  use { 'mg979/vim-visual-multi',
    branch = 'master',
  }

  -- twilight & zen
  use { "folke/twilight.nvim",
    opts = {
      -- your configuration comes here
      -- or leave it empty to use the default settings
      -- refer to the configuration section below
    }
  }
  use { "folke/zen-mode.nvim",
    opts = {
      -- your configuration comes here
      -- or leave it empty to use the default settings
      -- refer to the configuration section below
    }
  }

  -- Themes
  use {
    "goolord/alpha-nvim",
    config = function()
      require 'alpha'.setup(require 'core.dashboard'.config)
    end,
  }
  use { "EdenEast/nightfox.nvim" }
  use { "catppuccin/nvim", as = "catppuccin" }
  use {
    "norcalli/nvim-colorizer.lua",
    config = "require('core.colorizer')",
    event = "BufRead"
  }
  -- Windows animations
  use { 'xiyaowong/nvim-transparent' }
  use { "anuvyklack/windows.nvim",
    requires = {
      "anuvyklack/middleclass",
      "anuvyklack/animation.nvim"
    },
    config = function()
      vim.o.winwidth = 10
      vim.o.winminwidth = 10
      vim.o.equalalways = false
      require('windows').setup()
    end
  }
end)
